package com.example.Tony.chickenapplication.Data;

import android.content.Context;

import java.util.Date;

/**
 * Created by tony_ on 27/08/2016.
 */
public class ChickEntity extends Animal{
    private ChickenEntity _parent;

    public ChickenEntity get_parent() {
        return _parent;
    }

    public void set_parent(ChickenEntity _parent) {
        this._parent = _parent;
    }

    public ChickEntity() {
    }

    public ChickEntity(String _name, HouseEntity _house, Date _birthday, ChickenEntity _parent) {
        super(_name, _house, _birthday);
        this._parent = _parent;
    }

    //// TODO: 27/08/2016 override toString

}
