package com.example.Tony.chickenapplication.Data;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by tony_ on 27/08/2016.
 */
public class HouseEntity extends SugarRecord {

    private String _name;

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public HouseEntity() {
    }

    public HouseEntity(String name) {
        this._name = name;
    }

    //TODO: Ajouter liste d'animaux

}
