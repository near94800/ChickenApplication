package com.example.Tony.chickenapplication.Data;

import android.content.Context;

import java.util.Date;
import java.util.List;

/**
 * Created by tony_ on 27/08/2016.
 */
public class ChickenEntity extends Animal{
    private List<ChickEntity> _chicks;
    private List<EggEntity> _eggs;

    public List<ChickEntity> get_chicks() {
        return _chicks;
    }

    public void set_chicks(List<ChickEntity> _chicks) {
        this._chicks = _chicks;
    }

    public List<EggEntity> get_eggs() {
        return _eggs;
    }

    public void set_eggs(List<EggEntity> _eggs) {
        this._eggs = _eggs;
    }

    public ChickenEntity() {
    }

    public ChickenEntity(String _name, HouseEntity _house, Date _birthday, List<ChickEntity> _chicks, List<EggEntity> _eggs) {
        super(_name, _house, _birthday);
        this._chicks = _chicks;
        this._eggs = _eggs;
    }

    //// TODO: 27/08/2016 override toString
}
