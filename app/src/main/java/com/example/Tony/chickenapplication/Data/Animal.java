package com.example.Tony.chickenapplication.Data;

import android.content.Context;

import com.orm.SugarRecord;

import java.util.Date;

/**
 * Created by tony_ on 27/08/2016.
 */
public class Animal extends SugarRecord {

    protected String _name;
    protected HouseEntity _house;
    protected Date _birthday;

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public HouseEntity get_house() {
        return _house;
    }

    public void set_house(HouseEntity _house) {
        this._house = _house;
    }

    public Date get_birthday() {
        return _birthday;
    }

    public Animal() {
    }

    public void set_birthday(Date _birthday) {

        this._birthday = _birthday;
    }



    public Animal(String _name, HouseEntity _house, Date _birthday) {
        this._name = _name;
        this._house = _house;
        this._birthday = _birthday;
    }

    //// TODO: 27/08/2016 override toString

}
